<?php

echo "\n\nStarting build…\n\n";

$files = [];
foreach (new DirectoryIterator(__DIR__ . '/articles') as $file) {
    if ($file->isDir()) continue;
    $files[] = $file->getFileInfo();
}

usort($files, function (SplFileInfo $file_a, $file_b) {
    return $file_a->getBasename() < $file_b->getBasename();
});

$articles = [];
foreach ($files as $file) {
    echo "Building article {$file->getBasename()}…\n";
    $articles[] = build_article($file);
}

echo "\nBuilding index…\n";
build_index($articles);
echo "Building RSS…\n";
build_rss($articles);
echo "Building followed RSS…\n";
build_followed_rss();

echo "\n\nAll Done!\n\n";

function build_article($file)
{
    ob_start();
    include($file->getRealPath());
    $body = ob_get_clean();
    [$year, $month, $day] = explode('-', $file->getBasename());
    $human_date = "$day/$month/$year";
    $computer_date = "$year-$month-$day";
    $html_filename = str_replace('.php', '.html', $file->getBasename());

    ob_start();
    include(__DIR__ . '/template.php');
    $html = ob_get_clean();

    file_put_contents(__DIR__ . "/public/{$html_filename}", $html);

    return [
        'title' => $title,
        'filename' => $html_filename,
        'human_date' => $human_date,
        'computer_date' => $computer_date,
        'body' => $body,
    ];
}

function build_index($articles)
{
    ob_start();
    include(__DIR__ . '/index.php');
    $html = ob_get_clean();
 
    file_put_contents(__DIR__ . "/public/index.html", $html);
}

function build_rss($articles)
{
    $last_update_date = $articles[0]['computer_date'];
    foreach ($articles as $article) {
        if ($article['computer_date'] > $last_update_date) {
            $last_update_date = $article['computer_date'];
        }
    }

    ob_start();
    include(__DIR__ . '/atom.php');
    $html = ob_get_clean();
 
    file_put_contents(__DIR__ . "/public/atom.xml", $html);
}

function build_followed_rss()
{
    $xml = file_get_contents(__DIR__ . '/rss.xml');
    $rss = new SimpleXMLElement($xml);

    $flux_rss = [];
    foreach ($rss->body->outline as $category) {
        $name = (string) $category['text'];
        $flux_rss[$name] = $flux_rss[$name] ?? [];
        foreach ($category->outline as $some_rss) {
            $flux_rss[$name][] = [
                'title' => $some_rss['text'],
                'url' => $some_rss['htmlUrl'],
                'description' => $some_rss['description'],
            ];
        }
    }

    ob_start();
    include(__DIR__ . '/rss.php');
    $html = ob_get_clean();
 
    file_put_contents(__DIR__ . "/public/rss.php", $html);
}