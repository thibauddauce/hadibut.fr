<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <title><?= $title ?> — Réflexions d'Hadibut</title>
        <link rel="stylesheet" href="/style.css">
        <link rel="alternate" type="application/atom+xml" title="Réflexions d'Hadibut" href="./atom.xml">
        <meta property="og:title" content="<?= $title ?>">
        <meta property="og:url" content="https://hadibut.fr/<?= $html_filename ?>">
        <meta property="og:description" content="<?= $description ?>" />
        <meta name="description" content="<?= $description ?>" />

        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@ThibaudDauce" />
    </head>
    <body>
        <main>
            <h1><?= $title ?></h1>
            <p>Publié le <?= $human_date ?></p>

            <div>
                <?= $body ?>
            </div>
        </main>

        <footer>
            Si vous avez des questions, remarques, corrections, n'hésitez pas à me contacter via <a id="envoyé_à">thibaud[chez]hadibut.fr</a>
        </footer>
        <script>
            document.getElementById('envoyé_à').innerHTML = document.getElementById('envoyé_à').innerHTML.replace('[chez]', '@');
            document.getElementById('envoyé_à').href = 'mailto:' + document.getElementById('envoyé_à').innerHTML;

            document.querySelectorAll('iframe').forEach((iframe) => {
                console.log(iframe.offsetWidth);
                iframe.height = iframe.offsetWidth / (16/9);
            });
        </script>
    </body>
</html>
