<?php $title = "Les ordinateurs étaient plus rapides lorsqu'ils étaient plus lents" ?>
<?php $description = "Malgré l'augmentation de la puissance physique des ordinateurs    , les programmes ne sont pas plus rapides." ?>

<p>La puissance brute des ordinateurs n'a cessé d'augmenter, mais les programmes ne sont aujourd'hui pas plus rapides. La raison principale est que lorsque vous donnez plus de puissance hardware à un développeur, il va la plupart du temps la gaspiller pour obtenir un gain de productivité. La puissance hardware étant moins chère que le temps d'un développeur, il n'y a aucune incitation à faire des programmes performants.</p>

<iframe src="https://www.youtube.com/embed/V7lG4bont7g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>