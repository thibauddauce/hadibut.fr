<?php $title = 'Changer un écran pour un pixel mort ?' ?>
<?php $description = 'Les garanties sont un bon moyen de responsabiliser les fabricants mais génère également une pollution non négligeable.' ?>

<p>Je viens d'entendre quelqu'un qui veut faire marcher la garantie pour un pixel mort sur son ordinateur. Un bel ordinateur, presque neuf, à près de 2700 euros. Quel est le problème ?</p>

<p>Je ne sais pas si le service après-vente est capable de changer un unique pixel (corrigez-moi si je me trompe). Pour moi, l'écran entier sera remplacé. Est-ce que cela est économiquement viable ? Sans doute. Le fabricant doit intégrer le prix des réparations sous garantie dans son prix de vente. Mais est-ce que c'est écologiquement viable ? Est-ce qu'un ordinateur avec un pixel mort perd toute utilité ?</p>

<p>J'ai personnellement depuis plusieurs années un pixel mort en plein milieu de mon écran sur mon ordinateur portable. Ce n'est absolument pas gênant au quotidien, je m'en rends parfois compte lorsque je regarde des séries mais c'est tout. Mais est-ce que si je venais de l'acheter et qu'il était sous garantie j'aurais fait fonctionner celle-ci ? Je ne sais pas.</p>

<p>Un des seuls avantages que je vois à faire fonctionner la garantie c'est de responsabiliser le fabricant sur la qualité de ses produits. Si personne ne l'utilise, le fabricant peut décider d'utiliser des matériaux moins durables, car il n'a pas besoin de réparer les appareils en cas de problème.</p>

<p>Le mieux serait de pouvoir avoir le choix entre faire remplacer son écran, ou récupérer en cash l'argent de la réparation / du préjudice estimé. Cela permettrait à l'acheteur d'avoir une compensation sans pour autant remplacer inutilement un écran fonctionnel et de responsabiliser le fabricant qui perd économiquement quel que soit le choix.</p>