<?php $title = "Le sable : enquête sur une disparition" ?>
<?php $description = "Le sable, une ressource vitale dont le pillage s'accélère pour les besoins de la construction en béton. Avec pour conséquence principale l’érosion des littoraux." ?>

<iframe src="https://www.youtube.com/embed/xTWC8lWMTuQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<ul>
    <li>Le sable est présent dans les vins, dans le papier, la lessive, les cosmétiques, les appareils technologiques, plastique, peinture, pneu…</li>
    <li>Sur la planète les 2/3 de ce qui est construit, est construit en béton armé, le béton armé est constitué à 2/3 de sable.</li>
    <li>200 tonnes de sable pour construire une maison, 3 000 tonnes pour un bâtiment du type hôpital, 30 000 tonnes de sable par kilomètre d'autoroute.</li>
    <li>15 milliards de tonnes de sable utilisées par an, c'est la ressource la plus utilisée sur Terre après l'eau.</li>
    <li>Une proportion assez importante du fond de la mer est rocheux, ou recouvert d'une pellicule de sable très très fine qui a mis des dizaines de milliers d'années à se constituer.</li>
    <li>Une drague (un paquebot aspirateur de sable des fonds marins) peut déplacer entre 4 000 et 400 000 m3 par jour, chaque drague équivaut à un investissement entre 20 à 150 millions d'euros.</li>
    <li>Actuellement, plusieurs milliers de paquebots/dragues ratissent les fonds marins.</li>
    <li>Devant le béton, c'est le remblayage qui bat tous les records de consommation de sable.</li>
    <li>À Dubaï, ils ont découvert qu'il était moins cher de construire une île artificielle que d'acheter un terrain à terre.</li>
    <li>Le sable du désert est une richesse inutile pour les constructions quelqu'elle soit, au contraire, le sable marin s'agrège parfaitement.</li>
    <li>Quand on extrait du sable des fonds marins, les animaux et les plantes qui vivent là sont eux aussi aspirés, or, ces organismes constituent la base de la chaîne alimentaire des espèces qui vivent dans la colonne d'eau supérieure. Et tous les poissons dépendent des fonds marins pour trouver de la nourriture.</li>
    <li>L'un des effets les plus frappants du commerce du sable est la disparition de certaines îles en Indonésie. 25 îles indonésiennes ont déjà été rayées de la carte.</li>
</ul>
    <img src="/images/2019-09-23-le-sable-enquête-sur-une-disparition-erosion-ile.png" alt="Schéma d'explication de l'érosion des îles suite au pompage du sable.">
<ul>
    <li>Quand une île disparaît, les frontières maritimes internationales sont affectées. <em>Quelque chose qui est important je pense, mais auquel je n'avais jamais pensé.</em></li>
    <li>Singapour a agrandi sa superficie de 20% ces 40 dernières années.</li>
</ul>
    <img src="/images/2019-09-23-le-sable-enquête-sur-une-disparition-singapour.png" alt="Carte de l'augmentation de la superficie de Singapour en 2030.">
<ul>
    <li>La construction en bord de littoral empêche le sable de reculer pour absorber les chocs des vagues, et donc favorise l'érosion.</li>
</ul>
    <img src="/images/2019-09-23-le-sable-enquête-sur-une-disparition-construction.png" alt="Schéma d'explication du rôle des construction en bord de litoral sur l'érosion des plages">
<ul>
    <li>Il y a eu un projet de remblayage de plage en Californie qui a coûté 17 millions de dollars, et tout le sable a disparu à nouveau en moins d'un an. <em>Le montant n'est pas énorme sur des budgets réels, mais c'est a priori inutile, sauf pour faire venir les touristes une année de plus (les responsables de la plage de Miami ne s'en cachent pas trop dans le reportage).</em></li>
    <li>Les barrages empêchent le sable créé dans les montagnes de rejoindre la mer. <em>Je n'avais pas conscience de ce genre de problèmes avec l'hydraulique. C'est bien dommage, sachant que l'hydraulique est, avec le nucléaire, la méthode de production d'électricité pilotable la plus basse en émission de carbone.</em></li>
    <li>D'ici 2100, le niveau de l'eau augmentera entre 1 m et 1 m50. <em>Il existe de nombreux scénarios possibles, donc ces statistiques sont à prendre avec prudence, mais d'après ce que j'ai entendu des scénarios probables, il me semble que 1 m/1 m50 est plutôt une estimation basse.</em></li>
    <li>100 millions de personnes vivent entre 0m et 1 m au dessus du niveau de la mer.</li>
    <li>Quand vous prenez du sable et que vous le coulez dans du béton, ce sable est pris pour toujours, il n'existe plus comme ressource.</li>
    <li>Avec la paille qui est normalement brûlée après les moissons, on peut construire des immeubles en ballots de paille sans utiliser de ciment, sauf peut-être pour la dalle au sol. Ces maisons sont très bien isolées et résistent très bien aux incendies et aux séismes.</li>
    <li>On est accro au béton même si on l'aime pas, on ne peut pas y échapper car l'infrastructure économique et productive sont outillés pour construire avec du béton, ils savent construire avec du béton, et ils ne savent pas beaucoup construire avec d'autres matériaux. <em>C'est très intéressant, et vrai dans d'autres domaines. Il y a un vrai problème de formation dans beaucoup de domaines écologiques, avant même de changer les pratiques.</em></li>
    <li>Le verre, lorsqu'il est finement broyé, ressemble à s'y méprendre à du sable. Le sable de verre à toutes les caractéristiques physiques du sable ordinaire. <em>Je pensais qu'ils voulaient l'utiliser pour remplacer le sable dans le ciment, mais il est actuellement utilisé pour remblayer les plages.</em> Même les tortues marines l'ont adopté et acceptent d'y pondre leurs œufs.</li>
    <li>Le sable de verre est aujourd'hui plus cher que le sable des fonds marins. Il est difficile de faire concurrence à un produit très accessible et presque gratuit.</li>
    <li>Les sociétés de dragage viennent régulièrement se plaindre à Bruxelles de Natura 2000 et demander qu'on les autorise à exploiter les ressources qui se trouvent dans ces zones protégées.</li>
</ul>
