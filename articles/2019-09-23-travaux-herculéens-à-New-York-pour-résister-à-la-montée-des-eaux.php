<?php $title = "Travaux herculéens à New York pour résister à la montée des eaux" ?>
<?php $description = "Murets de sable à Manhattan et Brooklyn, dunes surélevées sur les plages les plus exposées, vastes chantiers à l'étude." ?>

<p>
    <a href="https://www.courrierinternational.com/depeche/travaux-herculeens-new-york-pour-resister-la-montee-des-eaux.afp.com.20190921.doc.1k96ny.xml">Courrier International : travaux herculéens à New York pour résister à la montée des eaux</a>
</p>

<p>Juste après avoir publié mon premier article sur les problèmes de la consommation du sable, je vois dans mon flux Twitter une dépêche de Courrier International, publiée hier, sur le coût des travaux nécessaires pour que la ville de New York résiste à la montée des eaux et aux tempêtes. Les coûts sont estimés entre 15 et 120 milliards de dollars.</p>