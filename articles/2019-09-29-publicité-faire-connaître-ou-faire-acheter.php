<?php $title = 'Publicité : « Faire connaître » ou « Faire acheter »' ?>
<?php $description = 'Est-ce que toutes les publicités poussent à la consommation ?' ?>

<p>Je suis globalement contre la publicité, mais je pense qu'il y a deux types majeurs de publicité.</p>

<p>La publicité afin de faire connaître sa marque, qui n'est pas forcément néfaste. Si personne ne connaît le produit révolutionnaire qui va changer votre façon de vivre de manière respectueuse de l'environnement, c'est dommage.</p>

<p>Ou alors la publicité qui est là pour vous inciter à acheter quelque chose dont vous n'avez pas besoin.</p>

<p>Prenons un exemple, lorsque Apple fait de la publicité pour son nouvel iPhone, ce n'est pas pour faire connaître la marque. N'importe quelle personne ayant besoin d'un nouveau téléphone, dans la première boutique ou même sur Internet, va directement se renseigner sur les derniers iPhones, les derniers Samsung, etc. La publicité n'est donc pas là pour convaincre les personnes ayant besoin d'un nouvel appareil mais bien pour inciter les personnes n'ayant aucune raison de changer d'acheter le nouveau.</p>
