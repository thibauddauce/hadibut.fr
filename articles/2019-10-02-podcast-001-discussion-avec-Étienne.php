<?php $title = "Podcast n°1 : discussion avec Étienne" ?>
<?php $description = "Expérimentation d'un podcast sur les problématiques écologiques" ?>

<p>Soyez indulgent, ceci est une expérimentation d'un podcast sur l'écologie que j'aimerais faire régulièrement :-) Je suis tout de même preneur des critiques permettant d'améliorer la qualité.</p>

<p>Si vous avez des idées, des réflexions sur le sujet, et que vous voulez en discuter, n'hésitez pas à me contacter pour qu'on organise un deuxième épisode.</p>

<audio src="/podcasts/001.mp3" controls></audio>

<p>Deux sources dont j'ai parlé :</p>

<p>
    <a href="https://www.youtube.com/watch?v=K01MnnOV-u4">L'effondrement de notre civilisation industrielle</a> de Absol Vidéos
</p>
<img src="/images/2019-10-02-podcast-001-matériaux-rares.jpg" alt="Durée restante des métaux rares">

<p>
    <a href="https://www.youtube.com/watch?v=Vjkq8V5rVy0">Jancovici : CO2 ou PIB, il faut choisir — Sciences Po</a> de Jean-Marc Jancovici (je dis une bêtise dans le podcast c'est 6% pour le ciment et non pas pour les aciéries qui sont dans les 11% de l'industrie)
</p>
<img src="/images/2019-10-02-podcast-001-émissions-carbone.jpg" alt="Répartition des émissions carbone dans le monde">