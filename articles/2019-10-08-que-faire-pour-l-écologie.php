<?php $title = "Que faire pour l'écologie ?" ?>
<?php $description = "Une petite liste personnelle d'actions vis-à-vis des problèmes écologiques." ?>

<p>Depuis que je m'intéresse à l'écologie, je me heurte à plusieurs problèmes.</p>

<p><strong>Premier problème </strong>: il n'y a pas de solution miracle. C'est extrêmement frustrant. Le problème étant global, touchant toutes nos activités, les solutions sont donc multiples.</p>

<p><strong>Deuxième problème</strong> : personne ne peut résoudre la situation écologique. L'action individuelle ne changera pas la donne, sauf si l'ensemble de la population (ou du moins une grande majorité) agit.</p>

<p><strong>Troisième problème</strong> : il est également impensable pour un individu de mettre en place toutes les solutions afin de rendre sa vie 100% « écologique ».</p>

<p>Maintenant, que faire ? Je ne peux pas vous donner une réponse à cette question, en fonction de votre vie, certaines solutions seront impossibles, d'autres seront triviales… Mais je peux vous proposer une liste non ordonnée des actions que je trouve pertinentes. Il y en a des simples pour moi qui seront compliquées pour vous, et inversement, et bien d'autres auxquelles je ne pense pas.</p>

<dl>
    <dt>Réduire sa consommation de viande (en particulier la viande du gros bétails)</dt>
    <dd>J'adore la viande, mais je peux m'en passer au quotidien sans problème.</dd>

    <dt>Réduire sa consommation de produit laitiers</dt>
    <dd>J'aime trop le fromage pour m'en passer. Mais j'ai arrêté le lait le matin il n'y a pas si longtemps.</dd>

    <dt>Manger local</dt>
    <dd>Je fais mes courses alimentaires dans un supermarché de producteurs de la région (bio et pas bio). Mais il y a toujours des choses qu'on ne trouve pas et pour lesquels on va à la Biocoop ou dans des supermarchés plus classiques.</dd>

    <dt>Réduire son utilisation de la voiture</dt>
    <dd>Pour les grandes distances je prends exclusivement le train/car. Pour les trajets en ville, je prends exclusivement les transports en commun. J'habite actuellement dans un village isolé de campagne et j'ai donc toujours besoin de faire mes courses en voiture ce qui me pose grandement problème actuellement.</dd>

    <dt>Ne plus prendre l'avion</dt>
    <dd>Mon dernier voyage à l'étranger était il y a 2 ans au Chili, je ne pense pas que je reprendrai l'avion dans ma vie mais seul l'avenir nous le dira. J'ai déjà énormément voyagé en avion étant petit, je pense avoir grillé mon quota.</dd>

    <dt>Ne pas avoir d'enfants</dt>
    <dd>Ça sera impossible pour moi car j'en veux depuis très longtemps. Je me conforte en disant que si tous les écologistes se retiennent d'avoir des enfants, la génération suivante ne sera pas bien éduquée.</dd>

    <dt>Réduire sa consommation d'appareils électroniques</dt>
    <dd>C'est mon métier et ma passion, je me vois donc mal me passer d'ordinateur ou de smartphone mais j'essaye de ne pas succomber au renouvellement compulsif d'appareils.</dd>

    <dt>Réduire sa consommation de contenus vidéos</dt>
    <dd>Je regarde énormément de vidéos sur Twitch/YouTube (conférences, lives, mais aussi beaucoup de conneries qui ne m'apportent rien…), ou des séries en replay ou sur Netflix. J'aimerais vraiment consommer moins de contenus, mais j'ai du mal à m'en détacher même si je ne perdrais rien à ne pas regarder le dernier test de l'iPhone que je n'utiliserai jamais… C'est une vraie addiction je pense.</dd>

    <dt>Réduire les douches</dt>
    <dd>J'aime trop l'eau chaude pour me passer de douche longue, mais j'essaye depuis quelques semaines d'en prendre uniquement une tous les deux jours.</dd>

    <dt>Bien isoler sa maison</dt>
    <dd>Je suis locataire, donc je peux pas trop travailler sur ce point pour le moment. En attendant, il vaut mieux porter un pull en intérieur plutôt que de monter le chauffage en hiver.</dd>

    <dt>Ne pas faire construire de nouvelle maison</dt>
    <dd>Une construction est extrêmement énergivore, il vaut mieux acheter une maison existante. De plus, les nouvelles constructions augmentent l'artificialisation des sols qui n'est pas une bonne chose pour la bio-diversité. Je ne suis pas encore dans ce choix personnellement.</dd>

    <dt>Choisir l'électricité en France</dt>
    <dd>En France, et grâce au nucléaire et dans une moindre mesure à l'hydraulique très présent, il vaut toujours mieux privilégier l'électricité au gaz ou au fuel. Même chose que le point précédent, n'étant pas propriétaire je n'ai pas beaucoup de marge de manœuvre sur ce point.</dd>

    <dt>Bien voter</dt>
    <dd>Ne pas voter pour des personnes qui compte augmenter la croissance, sachant que la croissance ira toujours de pair avec la consommation, et donc la pollution. En France, je n'ai pas l'impression d'avoir un choix évident sur ce point, entre les pro-croissances et les écologistes anti-nucléaire (ce qui est une bêtise sans nom scientifiquement)…</dd>

    <dt>Acheter moins de choses</dt>
    <dd>J'essaye d'attendre plusieurs semaines avant d'acheter quelque chose afin de vraiment réaliser si j'en ai besoin et ne pas acheter en magasin dans la précipitation.</dd>

    <dt>Réduire sa consommation de plastique</dt>
    <dd>Cette action rentre dans beaucoup des points précédents. En achetant local et non transformé j'ai généralement moins de plastique, mais c'est également vrai avec moins d'informatique, moins d'achats…</dd>

    <dt>Ne pas regarder de publicité</dt>
    <dd>J'évite comme la peste toutes les publicités en ne regardant pas la télévision, uniquement du replay, et avec un bloqueur de publicité <a href="https://addons.mozilla.org/fr/firefox/addon/ublock-origin/">uBlock Origin</a>. Même si je pense que la publicité n'a pas d'impact sur moi, je me fais avoir comme tout le monde à vouloir le dernier gadget et cela complexifie donc le <em>Acheter moins de choses</em>.</dd>

    <dt>Réduire ses déchets</dt>
    <dd>Avec le compost, et en me nourrissant principalement d'aliments non transformés, je n'ai que très peu de déchets.</dd>
</dl>

<p>Si vous avez d'autres idées, n'hésitez pas à me les partager sur Twitter ou par mail ! Bon courage à tous pour vos challenges personnels !</p>