php -S localhost:8000 -t public 2> /dev/null &
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

php build.php
while true; do
  change=$(inotifywait -re close_write,moved_to,create .)
  php build.php
done
