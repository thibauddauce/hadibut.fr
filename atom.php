<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Réflexions d'Hadibut</title>
    <link href="https://hadibut.fr/atom.xml" rel="self" />
    <link href="https://hadibut.fr" />
    <id>https://hadibut.fr/atom.xml</id>
    <author>
        <name>Thibaud Dauce</name>
        <email>thibaud@dauce.fr</email>
    </author>
    <updated><?= $last_update_date ?>T00:00:00Z</updated>
    <?php foreach ($articles as $article): ?>
        <entry>
            <title><![CDATA[<?= $article['title'] ?>]]></title>
            <link href="https://hadibut.fr/<?= $article['filename'] ?>" />
            <id>https://hadibut.fr/<?= $article['filename'] ?></id>
            <published><?= $article['computer_date'] ?>T00:00:00Z</published>
            <updated><?= $article['computer_date'] ?>T00:00:00Z</updated>
            <summary type="html"><![CDATA[<?= $article['body'] ?>]]></summary>
        </entry>
    <?php endforeach ?>
</feed>
