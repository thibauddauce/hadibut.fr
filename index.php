<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <title>Réflexions d'Hadibut</title>
        <link rel="stylesheet" href="/style.css">
        <link rel="alternate" type="application/atom+xml" title="Réflexions d'Hadibut" href="./atom.xml">
    </head>
    <body>
        <main>
            <h1>Réflexions d'Hadibut</h1>

            <p>Pour être notifié des nouveaux articles, suivez moi <a href="https://twitter.com/ThibaudDauce">@ThibaudDauce sur Twitter</a> ou ajoutez ce <a href="/atom.xml">flux RSS</a> à votre agrégateur de liens</p>

            <ul>
                <?php foreach ($articles as $article): ?>
                    <li><?= $article['human_date'] ?> — <a href="/<?= $article['filename'] ?>"><?= $article['title'] ?></a></li>    
                <?php endforeach ?>
            </ul>
        </main>
    </body>
</html>
